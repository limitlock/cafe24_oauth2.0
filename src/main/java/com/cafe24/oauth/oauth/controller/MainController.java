package com.cafe24.oauth.oauth.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * MainController
 */
@Controller
public class MainController {

    @GetMapping({"/",""})
	public String main(Model model) {
		model.addAttribute("g","aa");
		return "main";
	}



}