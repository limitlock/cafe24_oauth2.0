package com.cafe24.oauth.oauth.repository;

import com.cafe24.oauth.oauth.domain.Token;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OauthRepository extends JpaRepository<Token, Long>{
	
}